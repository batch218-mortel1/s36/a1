// This document contains our app feature in displaying and manipulating our databases

const Task = require("../models/task.js");

module.exports.getAllTask = () => {
	return Task.find({}).then(result => {
		return result;
	})
}

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name : requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false; //"Error detected"
		}
		else{
			return true;
		}
	})
}

// "taskId" parameter will serve as a storage of id in our url/link
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {

		if(err){
			console.log(err);
			return false;
		}
		else{
			return removedTask
		}

	})
};

module.exports.updateTask = (taskId) = (taskId, newEntry) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}

		result.name = newEntry.name;

		return result.save().then((updateTask, saveErr)=>{
			if(saveErr){
				console.log(saveErr);
				return false;
			}
			else{
				return updateTask;
			}
		})

	})

};

module.exports.getSpecificTask = (taskId) => {
	return Task.findOne(taskId).then(result => {
		return result;
	})
}

module.exports.updateSpecificTask = (taskId) = (taskId, newEntry) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}

		result.status = newEntry.status;

		return result.save().then((updateSpecificTask, saveErr)=>{
			if(saveErr){
				console.log(saveErr);
				return false;
			}
			else{
				return updateSpecificTask;
			}
		})

	})
};