/*
	GitBash:
		npm init -y
		npm i express
		npm i mongoose
		touch .gitignore >> content: node_modules
	
*/

//dependencies


const express = require("express");
const mongoose = require("mongoose"); 

/*
	// models folder >> task.js
	// contollers folder >> taskController.js
	// route folder >> taskRoute.js

*/
const taskRoute = require("./routes/taskRoute.js");

// Server Setup
const app = express(); 
const port = 3001;

//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// DB connection
mongoose.connect ("mongodb+srv://admin:admin@batch218-to-do.xqojp4d.mongodb.net/?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

app.use("/tasks", taskRoute);

app.listen(port, ()=> console.log(`Now listening to port ${port}`));

// GitBash:
// Start node application:
// nodemon index.js

