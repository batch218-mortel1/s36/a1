// This document contains all the endpoints for our application (also http methods)

const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController.js");

router.get("/viewTasks", (req, res) =>{
	// Invokes the "getAllTasks" function from the "taskController.js" file and sends the result back to 

								//returns result from our controller	
	taskController.getAllTask().then(result => res.send(result));
})

router.post("/addNewTask", (req, res) => {
	taskController.createTask(req.body).then(result => res.send(result))
})

router.delete("/deleteTask/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(result => res.send(result));
})

router.put("/updateTask/:id", (req, res) => {
	// req.params.id - will be the basis of what document we will update
	// req.body - the new documents/contents
	taskController.updateTask(req.params.id, req.body).then(result =>
		res.send(result));
})

router.get("/:id", (req, res) =>{
	taskController.getSpecificTask().then(result => res.send(result))
})

router.put("/:id/complete", (req, res) =>{
	taskController.updateSpecificTask(req.params.id, req.body).then(
		result => res.send(result))
})


module.exports = router;

/*[GET -Wildcard required]
1. Create a controller function for retrieving a specific task.
2. Create a route 
3. Return the result back to the client/Postman.
4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.

[PUT - Update - Wildcard required]
5. Create a controller function for changing the status of a task to "complete".
6. Create a route
7. Return the result back to the client/Postman.
8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.

9. Create a git repository named S31.
10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
11. Add the link in Boodle.
*/